console.log("Hello world");

// [SECTION] Syntax, Statements and Comments

// Statements in programming are instructions that we tell the computer to perform
// JS statements usually end with semicolon (;)
// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
// A syntax in programming, it is the set of rules that describes how statements must be constructed.
// All lines/blocks of code should be written in a specific manner/structure and sequence to work.

// Use double slash for single line comment

/* 
	Use slash asterisk to create a multi-line comment and end it with asterisk and slash
*/

// [SECTION] Variables
// It is used to contain data
console.log("-----------------");
console.log(">> Variables");

// Declaring variables
// Syntax
//	let/const variableName;
let myVariable;
console.log(myVariable);
//console.log is helpful for printing values or certain results of code into the browser's console

// undefined - value of variable is not defined

//let message = "Congrats B230 for completing Capstone 1";
// console.log(message);
// let message = "Congrats B230 for completing Capstone 1";

// Variables must be declared first before they are used
// Using variables before they're declared will return an error

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

*/

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given it's initial/starting value 
//Syntax
	// let/const variableName = value;

let message = "Hooray, I finished Capstone 1";
console.log(message);

/*
	let message; // Declaration
	message = "Hooray, ..." // Initialization
	console.log(message); // Display output
*/

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain applications, some variables/information are constant and should not be changed
// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended

const interest = 3.539;

// Reassigning variable values
// Reassigning a variable means changing it's initial or previous value into another value

// Syntax
    // variableName = newValue;

console.log("-----------------");
console.log(">> Re-assigning variable values");

productName = 'Laptop';
console.log(productName);

// Declares a variable first
let supplier;

supplier = "John Smith Tradings";
console.log(supplier);

//Re-assignment
supplier ="Zuitt Store";
console.log(supplier);

/*
const pi;
pi = 3.1416;
console.log(pi);

// Error reassigning a variable declared with const

*/

/*const pi = 3.1416;
pi = 5.1;
console.log(pi);
*/

// Error reassigning a variable declared with const

console.log("-----------------");
console.log(">> var vs let/const ");

//var - used ES1
//let/const - used ES6
//issues associated with variables declared with var, regarding hoisting.

//Hoisting is JS's default behavior of moving declarations to the top.

a = 5;
console.log(a);
var a;

// console.log(num);
// var num = 51;

// -----------------------

//Multiple Variable Declarations
console.log("-----------------");
console.log(">> Multiple Variable Declarations");

//Multiple variables may be declared in one line
let x, y;
x = "1";
y = "2";
console.log(x,y);

let productCode = "DC017";
let productBrand = "Dell";
console.log(productCode, productBrand);

//Using reserved keyword as variable
console.log("-----------------");
console.log(">> Using reserved keyword as variable");

// const let = "hello";
// console.log(let);

// [SECTION] Data Types
console.log("-----------------");
console.log(">> [Data Type] >> String");

// Strings

// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote
// In other programming languages, only the double quotes can be used for creating strings

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ', ' + country;
console.log(fullAddress);

console.log("-----------------");
console.log(">> Escape character");

// The escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

console.log("-----------------");
console.log(">> Output with quotation marks");
// Using double quotes along with single can allow to easily include single quotes in texts without using escape character

let updateMessage = "John's employees went home early"; 
console.log(updateMessage);
updateMessage = 'John\'s employees went home early';
console.log(updateMessage);

// Numbers
console.log("------------");
console.log(">> [Data Type] Numbers ");

// Integers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation (e)
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and string will result to string output (Coercion)
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to state or certain things

// This will be useful in further discussions about creating logic to make our application respond to certain scenarios

console.log("--------------");
console.log(">> [Data Type] Boolean");

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types

console.log("--------------");
console.log(">> Arrays ");

// Syntax
// let/const arrayName = [elementA, elementB, elementC];
// In arrays, we call our values elements

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
//storing different data types inside an array is not recommended because it will not make sense to in the context of programming
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world object/items
// They're used to create complex data that contains pieces of info that are relevant to each other
// Every individual piece of info is called a 'property' of the object
// Syntax

/*

	let/const objectName = {
		propertyA: value,
		propertyB: value
	}

*/

console.log("------------");
console.log(">> [Data Type]  Objects");

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}
console.log(myGrades);

// typeof operator is used to determine the type of data or the value of the variable. It outputs a string
console.log(typeof myGrades);

console.log(typeof grades);

// Constant Objects and Arrays

console.log("------------");
console.log(">> Constant Objects and Arrays ");

// elements are assigned per index
// index starts with zero
const anime = ["one piece", "Kimetsu no Yaiba", "Dragon Ball", "Spy X Family", "Gintama"];
console.log(anime);
console.log(anime[0]);

anime[0] = "Bleach";
console.log(anime);

// const pokemon = ['pikachu', 'meowth', 'charizad'];
// pokemon = ['raichu', 'ditto', 'kakuna'];
// console.log(pokemon);

// We cannot reassign the whole value of the variable, but we can change the elements of the constant array.

console.log("------------");
console.log(">> Null and Undefined ");

// Null
// It is used to intentionally express the absence of a value in a variable declaration/ initialization
// null simply means that a data type was assigned to a variable but it does not hold any value or is nullified

let spouse = null;
console.log(spouse);

// Using null compared to a 0 value and an empty string is much better for readability purposes
// null is also considered as a data type of it's own compared to 0 which is a data type of a number and single quotes which are a data type of a string

// let myString = 0;
// let myString = '';

// Undefined
// Represents the state of a variable that has been declared but without an assigned value

// let fullName;
// console.log(fullName);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing


